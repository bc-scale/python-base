ARG CI_REGISTRY=registry.gitlab.com
ARG BASE_IMAGE_NAME=marwamc/images/ubuntu-base
ARG BASE_IMAGE_TAG=focal-latest

FROM ${CI_REGISTRY}/${BASE_IMAGE_NAME}:${BASE_IMAGE_TAG}

ARG PYTHON_VERSION=python3.10
ARG POETRY_VERSION=1.1.13
ARG POETRY_CORE_VERSION=1.0.8

ENV PIP_DISABLE_PIP_VERSION_CHECK=1 \
    POETRY_VIRTUALENVS_IN_PROJECT=true \
    PATH="${PATH}:${HOME}/.local/bin/"

USER root

RUN set -ex \
    && apt-get -y update \
    && apt-get install -y software-properties-common \
    && add-apt-repository -y ppa:deadsnakes/ppa \
    && apt-get -y update \
    && apt-get install -y \
        ${PYTHON_VERSION} \
        ${PYTHON_VERSION}-distutils \
    && apt-get clean all \
    && rm -rf /var/lib/apt/lists/* \
    && ln -sf /usr/bin/${PYTHON_VERSION} /usr/bin/python3

USER 1000

RUN set -ex \
    && curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py \
    && python3 get-pip.py --user \
    && rm -f /tmp/get-pip.py \
    && pip install \
        poetry==${POETRY_VERSION} \
        poetry-core==${POETRY_CORE_VERSION} \
    && pip cache purge
